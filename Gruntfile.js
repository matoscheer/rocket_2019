module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        manage: false
      },
      my_target: {
        files: {
          'public/js/app.min.js': ['sources/js/*.js']
        }
      }
    },
    postcss: {
      options: {
        map: true, // inline sourcemaps
        processors: [
          require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')({
            browsers: [
              "> 0.3%",
              "last 5 versions",
              "Android >= 4",
              "Firefox >= 20",
              "iOS >= 6"
            ],
            flexbox: true,
          }), // add vendor prefixes
          require('cssnano')() // minify the result
        ]
      },
      dist: {
        src: 'public/css/*.css'
      }
    },
    sass: {
      dist: {
        options:{
          // style:'compact'
          style:'compressed'
        },
        files: {
          'public/css/screen.css' : 'sources/scss/screen.scss'
        }
      }
    },
    // autoprefixer:{
    //   dist:{
    //     files:{
    //       'public/css/screen.css':'public/css/screen.css'
    //     }
    //   }
    // },
    watch: {
      css: {
        files: 'sources/scss/**',
        tasks: ['sass', 'postcss']
      },
      js:{
        files: 'sources/js/*.js',
        tasks: ['uglify'],
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.registerTask('default',['watch']);
};

