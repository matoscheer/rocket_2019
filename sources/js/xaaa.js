var counter = $('#counter');
$(window).scroll(startCounter);
function startCounter() {
    if (counter.hasClass('animated')) {
        $(window).off("scroll", startCounter);
        animateValue("value", 0, 30, 2500);
    }
}
function animateValue(id, start, end, duration) {

    var obj = document.getElementById('counter');
    var range = end - start;
    var minTimer = 5;
    var stepTime = Math.abs(Math.floor(duration / range));
    stepTime = Math.max(stepTime, minTimer);
    var startTime = new Date().getTime();
    var endTime = startTime + duration;
    var timer;
    function run() {
        var now = new Date().getTime();
        var remaining = Math.max((endTime - now) / duration, 0);
        var value = Math.round(end - (remaining * range));
        obj.innerHTML = value;
        if (value == end) {
            clearInterval(timer);
            obj.innerHTML = numberWithSpaces(value);
        }
    }

    timer = setInterval(run, stepTime);
    run();
}

function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}
