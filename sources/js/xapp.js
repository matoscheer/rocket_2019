$(document).ready(function() {
    new WOW({
        mobile: true,
        live: false,
        duration: '1.5s',
        offset: 70,
    }).init();

    $(".js-burger").on('click', function() {
        $('#burger-check').prop('checked', false);
    });
});
