<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactFormType;
use App\Notifications\ContactNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/** @Route({
 *     "en": "/en",
 *     "zh": "/zh"
 * })
 */

class ArticleController extends AbstractController
{
    protected $recipientEmail;

    public function __construct(string $recipientEmail)
    {
        $this->recipientEmail = $recipientEmail;
    }

    /**
     * @Route("/", name="app_homepage")
     * @return Response
     */
    public function homepage()
    {
        return $this->render('pages/homepage.html.twig', [
            'title' => 'Rocket It homepage',
        ]);
    }

    /**
     * @Route("/products", name="app_products")
     */
    public function products()
    {
        return $this->render('pages/_products.html.twig', [
            'title' => 'Rocket IT Products'
        ]);
    }

    /**
     * @Route("/features", name="app_features")
     */
    public function features()
    {
        return $this->render('pages/_features.html.twig', [
            'title' => 'Rocket IT features',
        ]);
    }

    /**
     * @Route("/partnership", name="app_partnership")
     */
    public function partnership()
    {
        return $this->render('pages/_partnership.html.twig', [
            'title' => 'Rocket IT partnership',
        ]);
    }

    /**
     * @Route("/contact", name="app_contact")
     * @return Response
     */
    public function contact(Request $request, ContactNotification $notification, \Swift_Mailer $mailer):Response
    {

        $contact = new Contact();
        $form = $this->createForm(ContactFormType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $notification->notify($contact);
            $contact = $form->getData();

            $emailMessage = (new \Swift_Message('contact request'))
                ->setFrom($contact->getEmail())
                ->setTo($this->recipientEmail)
                ->setBody($this->renderView(
                    'emails/contact.html.twig',[
                        'contact' => $contact
                    ]
                ),
                    'text/html');

            $mailer->send($emailMessage);

            $this->addFlash('success animate', 'Your email was sent successful.');
            return $this->redirectToRoute('app_contact');
        } elseif($form->isSubmitted() && $form->isValid() === false) {
            $this->addFlash('danger animate', 'Your email was not sent! Please fill all required fields!');
        }
        return $this->render('pages/_contact.html.twig', [
            'title' => 'Rocket IT contact',
            'contact_form_captcha' => $form->createView(),
            'errors' => $form->getErrors(true, true),
        ]);
    }

    /**
     * @Route("/about", name="app_about")
     */
    public function about()
    {
        return $this->render('pages/_about.html.twig', [
            'title' => 'About Rocket IT',
        ]);
    }

    /**
     * @Route("/terms-of-use", name="app_terms")
     */
    public function terms()
    {
        return $this->render('pages/_terms.html.twig', [
            'title' => 'About Rocket IT Terms of Use',
        ]);
    }
}
