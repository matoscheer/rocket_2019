<?php
namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Contact {

    /**
     * @var string|null
     * @Assert\NotBlank()
     */
    private $fullname;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string|null
     */
    private $companyname;

    /**
     * @var string|null
     * @Assert\NotBlank()
     */
    private $message;

    /**
     * @var boolean|null
     * @Assert\IsTrue()
     */
    private $agreement;

    /**
     * @return string|null
     */
    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    /**
     * @param string|null $fullname
     * @return Contact
     */
    public function setFullname(?string $fullname): Contact
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Contact
     */
    public function setEmail(?string $email): Contact
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCompanyname(): ?string
    {
        return $this->companyname;
    }

    /**
     * @param string|null $companyname
     * @return Contact
     */
    public function setCompanyname(?string $companyname): Contact
    {
        $this->companyname = $companyname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     * @return Contact
     */
    public function setMessage(?string $message): Contact
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getAgreement(): ?bool
    {
        return $this->agreement;
    }

    /**
     * @param bool|null $agreement
     * @return Contact
     */
    public function setAgreement(?bool $agreement): Contact
    {
        $this->agreement = $agreement;
        return $this;
    }
}
