<?php

namespace App\Form;


use App\Entity\Contact;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, [
                'attr' => ['placeholder' => 'full_name']
            ])
            ->add('email', EmailType::class, [
                'attr' => ['placeholder' => 'email_address']
            ])
            ->add('companyname', TextType::class, [
                'attr' => ['placeholder' => 'company_name']
            ])
            ->add('message', TextareaType::class, [
                'attr' => ['placeholder' => 'your_messages', 'rows' => '12'],
            ])
            ->add('agreement', CheckboxType::class)
            ->add('recaptcha', EWZRecaptchaType::class, array(
                'label' => '',
                'mapped' => false,
                'language' => 'en',
                'attr' => array(
                    'options' => array(
                        'language' => 'en',
                        'theme' => 'light',
                        'type'  => 'image',
                        'size'  => 'normal',
                        'defer' => true,
                        'async' => true,
                        'bind' => 'contact_form_submit'
                    )
                ),
                'constraints' => [new IsTrue()]
            ))
            ->add('submit', SubmitType::class, ['label' => 'send_message', 'attr' => ['class' => 'btn_huge']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class
        ]);
    }

}
